# frozen_string_literal: true

require_relative "lib/rsocks5patch/version"

Gem::Specification.new do |spec|
  spec.name = "rsocks5patch"
  spec.version = Rsocks5patch::VERSION
  spec.authors = ["Sam Baskinger"]
  spec.email = ["basking2@yahoo.com"]

  spec.summary = "Patch SOCKS5 capabilities into Ruby's Socket class."
  # spec.description = "Patch SOCKS5 capabilities into Ruby's Socket class."
  spec.homepage = "https://gitlab.com/basking2/rsocks5patch"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/basking2/rsocks5patch"
  spec.metadata["changelog_uri"] = "https://gitlab.com/basking2/rsocks5patch/-/blob/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
