# Rsocks5patch

When including `rsocks5patch` the Ruby `Socket` class function `tcp`
will be replaced with one that will first establish a connection
through a SOCKS proxy.

The new `Socket.tcp` function also adds two options, `socks_host`
and `socks_port` in situations where the `tcp` function is used directly.

An example use.

```ruby

require "rsocks5patch"
Socket.socks_host = "localhost"
Socket.socks_port = 9050

resp = Net::HTTP.get_response(URI("https://gitlab.com/basking2/rsocks5patch/-/tree/main"))
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).