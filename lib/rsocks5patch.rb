# frozen_string_literal: true

# vim: set et sw=2:

require "net/http"
require "uri"
require "socket"

require_relative "rsocks5patch/version"
require_relative "rsocks5patch/socks5"

# When this module is evaluated it will patch the Ruby Socket class.
module Rsocks5patch
  class Error < StandardError; end

  Socket.class_eval do
    # alias :orig_func :self.tcp
    #
    class << self
      alias_method :orig_func, :tcp

      attr_accessor :socks_host, :socks_port
    end

    def self.tcp(
      host,
      port,
      local_host = nil,
      local_port = nil,
      connect_timeout: nil,
      resolv_timeout: nil,
      socks_host: nil,
      socks_port: nil
    )
      socks_host ||= self.socks_host
      socks_port ||= self.socks_port

      unless socks_host
        return orig_func(host, port, local_host, local_port, connect_timeout: connect_timeout,
                                                             resolv_timeout: resolv_timeout)
      end

      socket = orig_func(socks_host, socks_port, local_host, local_port, connect_timeout: nil, resolv_timeout: nil)

      Rsocks5patch::Socks5.init_socks5(socket, host, port)

      socket
    end
  end

  TCPSocket.class_eval do
    class << self
      alias_method :orig_func, :open
    end

    def self.open(
      conn_addr,
      conn_port,
      local_host = nil,
      local_port = nil,
      connect_timeout: nil,
      resolv_timeout: nil,
      socks_host: nil,
      socks_port: nil
    )
      socks_host ||= Socket.socks_host
      socks_port ||= Socket.socks_port

      unless socks_host
        return orig_func(conn_addr, conn_port, local_host, local_port, connect_timeout: connect_timeout,
                                                                       resolv_timeout: resolv_timeout)
      end

      socket = orig_func(socks_host, socks_port, local_host, local_port, connect_timeout: nil, resolv_timeout: nil)

      Rsocks5patch::Socks5.init_socks5(socket, conn_addr, conn_port)

      socket
    end
  end
end
