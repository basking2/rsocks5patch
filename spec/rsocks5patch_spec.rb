# frozen_string_literal: true

RSpec.describe Rsocks5patch do
  it "has a version number" do
    expect(Rsocks5patch::VERSION).not_to be nil
  end

  it "does something useful" do
    expect(true).to eq(true)
  end

  it "uses a tor proxy if one is running" do
    Socket.socks_host = "localhost"
    Socket.socks_port = 9050

    _resp = Net::HTTP.get_response(URI("https://gitlab.com/basking2/rsocks5patch/-/tree/main"))
  rescue Errno::ECONNREFUSED
    # ECONNREFUSED is actually a form of success.
    # If no tor proxy but we attempt to connect to localhost
    # then that's success.
  end

  it "doesn't use a tor proxy if one is running" do
    Socket.socks_host = nil
    Socket.socks_port = nil

    _resp = Net::HTTP.get_response(URI("https://gitlab.com/basking2/rsocks5patch/-/tree/main"))
  end
end
